import sqlite3
import zipfile
import csv
import io
import zipfile
from datetime import datetime, timezone
from tqdm import tqdm_notebook as tqdm
import json
db = sqlite3.connect('RoeeDB')
cursor = db.cursor()
cursor.execute("DROP TABLE IF EXISTS trips ")
cursor.execute('''CREATE TABLE IF NOT EXISTS trips(TRIP_ID, CALL_TYPE, ORIGIN_CALL, ORIGIN_STAND, MISSING_DATA, 
                  TAXI_ID, TIMESTAMP, DATE, HOUR, MINUTE, TRIP_DURATION);''')

import io
import csv
import zipfile
from tqdm import tqdm_notebook as tqdm

COMMIT_EVERY = 10000
TOTAL_COMMIT = 100000

INSERT = """INSERT INTO trips
        (TRIP_ID,
        CALL_TYPE,
        ORIGIN_CALL,
        ORIGIN_STAND,
        MISSING_DATA,
        TAXI_ID,
        TIMESTAMP,
        DATE,
        HOUR,
        MINUTE,
        TRIP_DURATION)
        VALUES(?,?,?,?,?,?,?,?,?, ?, ?)"""

with zipfile.ZipFile('train.csv.zip') as z:
    with z.open('train.csv', 'r') as f:
        textwrap = io.TextIOWrapper(f, encoding='utf8', newline='')
        reader = csv.DictReader(textwrap, delimiter=",")
        i = 0
        j = 0
        committed = 0
        for row in reader:
            cursor.execute(INSERT,
                           (
                               int(row['TRIP_ID']),
                               row['CALL_TYPE'],
                               row['ORIGIN_CALL'],
                               row['ORIGIN_STAND'],
                               row['MISSING_DATA'],
                               int(row['TAXI_ID']),
                               datetime.fromtimestamp(int(row['TIMESTAMP'])),
                               datetime.fromtimestamp(int(row['TIMESTAMP'])).strftime('%m %d'),
                               int(datetime.fromtimestamp(int(row['TIMESTAMP'])).strftime('%H')),
                               int(datetime.fromtimestamp(int(row['TIMESTAMP'])).strftime('%M')),
                               (len(json.loads(row['POLYLINE'])) - 1) * 15
                           ))

            i += 1
            j += 1
            if j % 100 == 0:
                print("gone over {} rows".format(j))
            if j > TOTAL_COMMIT:
                break

            if i == COMMIT_EVERY:
                db.commit()
                print(committed)
                committed += i
                i = 0

    if i > 0:
        db.commit()
cursor.close()
